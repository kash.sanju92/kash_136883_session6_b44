<?php

//backslash

//echo "how are "you""; // eta print hobe na... coz ekhane " " ke thik vabe pabe na

echo "how are \"you\""; // evabe likhte hobe

echo "<br> i am 'kashfia'"; // etar jonno backslash lagbe na

$myStr= addslashes('<br>hello kash "37" <br> how are "YoU" '); /* eta backslash boshiye debe.
  boro paragraph er khetre
 */
echo $myStr;

 echo "hello kash \"37\"";
 echo "how are \"YoU\"";

$myString= addslashes('<br>hello kash "37" <br> how are \"YoU\" <br> my age \24\ <br> i am \'hungy\'<br>');
// eta backslash boshiye debe.

echo $myString;

// explode: string to array
//string ke arroay banay

$forEx =  "hello world! how is life";
$exArray = explode(" ", $forEx);// " " mane space er vittite alada korbo. $forEx mane ai string ke array koro

print_r($exArray);
//spliting string ke array akare ke print korbe
echo "<br>";

//implode: array to string

$forIm = implode("*#", $exArray);
echo $forIm;
echo "<br>";

//htmlentitites: string er moddhe html entiti thakle take output e dekhanor kaj korbe
$html = '<br> means line break';
$showhtml = htmlentities($html);
echo $showhtml;